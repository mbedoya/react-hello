import React, { Component } from 'react';

import logo from './logo.svg';
import './App.css';
import SingleTodo from "./SingleTodo";

class App extends Component {
  constructor() {
    super();
    this.state = {
      todos: [],
      count: 0,
      currentTodo: ""
    }
  }

  onTodoInputChange = (e) => {
    this.setState({ currentTodo: e.target.value });
  }

  onIncrement = () => {

    let todosCopy = this.state.todos.slice();
    todosCopy.push(this.state.currentTodo);

    this.setState({ count: this.state.count + 1, currentTodo: "", todos: todosCopy });
  }

  onDeleteTodo = (index) => {
    let todosCopy = this.state.todos.slice();
    todosCopy.splice(index, 1);
    this.setState({ count: this.state.count -1, todos: todosCopy });
  }

  render() {

    let todosView = this.state.todos.map(( todo, index) => {
      return (
        <SingleTodo todo={todo} delete={ () => this.onDeleteTodo(index) } />
      );
    });

    return (    
      <div>
        <div>Hello</div>
        <input type="text" onChange={this.onTodoInputChange} value={this.state.currentTodo} ></input>
        <button onClick={this.onIncrement}>Increment</button>
        <p>
          {this.state.count}
        </p>
        <p>
          Todos: <br />
          {this.state.todos.length}
          <ul>
            {todosView}
          </ul>
        </p>
      </div>
    );
  }
}

export default App;
